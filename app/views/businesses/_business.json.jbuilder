json.extract! business, :id, :business_type, :name, :phone_number, :alt_phone_number, :email, :address, :logo, :user_id, :created_at, :updated_at
json.url business_url(business, format: :json)
