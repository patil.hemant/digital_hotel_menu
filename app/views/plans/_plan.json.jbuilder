json.extract! plan, :id, :name, :amount, :description, :no_of_days, :no_of_printable_qrcodes, :created_at, :updated_at
json.url plan_url(plan, format: :json)
