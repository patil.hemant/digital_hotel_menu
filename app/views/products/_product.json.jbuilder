json.extract! product, :id, :category_id, :name, :food_type, :half_price, :full_price, :ac_half_price, :ac_full_price, :description, :product_type_id, :business_id, :unit, :price, :created_at, :updated_at
json.url product_url(product, format: :json)
