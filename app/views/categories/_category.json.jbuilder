json.extract! category, :id, :name, :display_sequence, :business_id, :created_at, :updated_at
json.url category_url(category, format: :json)
