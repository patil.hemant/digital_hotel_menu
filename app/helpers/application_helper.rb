module ApplicationHelper

    def get_qr_code(text)
        require 'rqrcode'

        qrcode = RQRCode::QRCode.new(text)
    
        qrcode.as_svg(
          offset: 0,
          color: '000',
          shape_rendering: 'crispEdges',
          module_size: 6,
          standalone: true
        )
    end
end
