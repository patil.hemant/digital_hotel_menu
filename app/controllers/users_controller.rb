class UsersController < ApplicationController
  
  before_action :authenticate_user!

  def edit
    @user = current_user
  end

  def update_password
    @user = current_user
    if @user.update_with_password(user_params)
      # Sign in the user by passing validation in case their password changed
      bypass_sign_in(@user)
      redirect_to root_path
    else
      render "edit"
    end
  end

  #  def update_password
  #   if params[:update_user_id].present?
  #     @user = User.find_by_id(params[:update_user_id])
  #     @user.errors.add(:email, "Please enter current email") if params[:current_email].blank?
  #     @user.errors.add(:email, "Current email doesn't match") if @user.email != params[:current_email]
  #     @user.update_without_password(user_params) if @user.errors.blank?
  #   else
  #     @user = current_user
  #     binding.pry
  #     params[:user][:password] = params[:user][:current_password] if params[:user][:password].blank? && @user.valid_password?(params[:user][:current_password])
  #     if @user.update_with_password(user_params)
  #       sign_in @user, bypass: true
  #     else
  #       @user.errors[:password] << t('errors.messages.blank') if params[:user][:password].blank?
  #     end
  #   end  
  #   render "edit" if @user.errors.present?
  #   # respond_to do |format|
  #   #     format.js
  #   #     format.html
  #   # end
  # end

  private

  def user_params
    # NOTE: Using `strong_parameters` gem
    params.require(:user).permit(:password, :password_confirmation, :current_password)
  end
end