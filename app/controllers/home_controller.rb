class HomeController < ApplicationController
  skip_before_action :authenticate_user!
  layout :render_layout


  def index
  	if user_signed_in?
  		@active_plan = current_user.active_plan rescue nil
  	end
  end

  def home_1
	render :layout=>'home'
  end

  def render_layout
	  if user_signed_in?
	    'application'
	  else
	    'home_page_layout'
	  end
	end
end
