class PurchasesController < ApplicationController
	skip_before_action :verify_authenticity_token
	def create
		begin
	      @business_plan = BusinessPlan.process_razorpayment(payment_params)
	      # redirect_to :action => "show", :id => @order.id
	      session[:suscribed] = true
	      redirect_to root_path
	    rescue Exception
	      flash[:alert] = "Unable to process payment."
	      redirect_to root_path
	    end
	end

	private
    def payment_params
      p = params.permit(:user_id, :plan_id, :razorpay_payment_id)
      p.merge!({payment_id: p.delete(:razorpay_payment_id) || p[:payment_id]})
      p
    end

end
