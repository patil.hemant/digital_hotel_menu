class Category < ApplicationRecord
    	
    validates :name, presence: { message: "Category name is required." }
    # validates :display_sequence, uniqueness: { message: "This display sequence is already taken." }

    belongs_to :business
    has_many :products
end
