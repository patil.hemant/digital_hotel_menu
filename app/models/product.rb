class Product < ApplicationRecord
    belongs_to :category
    belongs_to :business
    belongs_to :product_type, :optional=> true
end
