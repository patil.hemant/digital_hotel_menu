class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  has_one :business
  accepts_nested_attributes_for  :business, allow_destroy: true


  # after_create :create_business
  
  def active_plan
    self.business.business_plans.where(active: true).last.plan
  end

  def create_business
    Business.create(:user_id=>self.id)
  end

end
