class Business < ApplicationRecord
    has_many :categories
    belongs_to :user 
    has_many :products
    has_many :business_plans
    mount_uploader :logo, LogoUploader

    accepts_nested_attributes_for   :categories

    validates :name, presence: { message: "Business name is required." }
    validates :name, format: {:with => /\A[^0-9`!@#\$%\^&*+_=]+\z/, message: "Business name is invalid"}
    validates :phone_number, presence: { message: "Phone number is required." }
    validates :phone_number, format: {:with => /[2-9]{2}\d{8}/, message: "Should contain only 10 digits"}
    validates :address, presence: { message: "Address required." }
end
