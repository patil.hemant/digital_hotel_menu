class Plan < ApplicationRecord
    has_many :business_plans
    def get_amount_in_paisa
      amount = self.amount.to_s.split('.')
      rupee = amount.first
      paisa = amount.last
      final_amt = (rupee.to_i * 100) + paisa.to_i     
    end

    def get_subscription_end_date
    	Date.today + self.no_of_days
    end

end
