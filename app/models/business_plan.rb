class BusinessPlan < ApplicationRecord
    include PurchaseConcerns::Razorpay
    belongs_to :business
    belongs_to :plan


    class << self
	    def process_razorpayment(params)
	      @plan = Plan.find(params[:plan_id])
	      @user = User.find(params[:user_id])
	      @transaction_id =  params[:payment_id]
	      @razorpay_pmnt_obj = fetch_payment(@transaction_id)
	      status = @razorpay_pmnt_obj.status
	      if status == "authorized"
          @razorpay_pmnt_obj.capture({amount: @plan.get_amount_in_paisa})
          @razorpay_pmnt_obj = fetch_payment(@transaction_id)
          @business_plan = BusinessPlan.create_busniess_plan_record
          @business_plan
	      else
	        raise StandardError, "Unable to capture payment"
	      end
	    end

	    # def process_refund(payment_id)
	    #   fetch_payment(payment_id).refund
	    #   record = Order.find_by_payment_id(payment_id)
	    #   record.update_attributes(status: fetch_payment(payment_id).status)
	    #   return record
	    # end

	    # def filter(params)
	    #   scope = params[:status] ? Order.send(params[:status]) : Order.authorized
	    #   return scope
	    # end

  	end

  	def self.create_busniess_plan_record
	    @user.business.business_plans.update_all(active: false)
	    obj = ({
	      business_id: @user.business.id,
	      plan_id: @plan.id,
	      status: @razorpay_pmnt_obj.status, 
	      price: @plan.amount, 
	      transaction_id: @transaction_id
	    })
	    BusinessPlan.create(obj)
	  end


end
