class CreateBusinesses < ActiveRecord::Migration[5.1]
  def change
    create_table :businesses do |t|
      t.string :business_type
      t.string :name
      t.string :phone_number
      t.string :alt_phone_number
      t.string :email
      t.text :address
      t.text :logo
      t.integer :user_id

      t.timestamps
    end
    add_index :businesses, :user_id
  end
end
