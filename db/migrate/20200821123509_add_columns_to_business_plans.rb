class AddColumnsToBusinessPlans < ActiveRecord::Migration[5.1]
  def change
    add_column :business_plans, :status, :string
    add_column :business_plans, :transaction_id, :string
  end
end
