class CreateCategories < ActiveRecord::Migration[5.1]
  def change
    create_table :categories do |t|
      t.string :name
      t.integer :display_sequence
      t.integer :business_id

      t.timestamps
    end
    add_index :categories, :business_id
  end
end
