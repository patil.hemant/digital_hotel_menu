class AddPriceToBusinessPlans < ActiveRecord::Migration[5.1]
  def change
    add_column :business_plans, :price, :float
  end
end
