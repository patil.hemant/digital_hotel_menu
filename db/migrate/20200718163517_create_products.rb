class CreateProducts < ActiveRecord::Migration[5.1]
  def change
    create_table :products do |t|
      t.integer :category_id
      t.string :name
      t.string :food_type
      t.float :half_price
      t.float :full_price
      t.float :ac_half_price
      t.float :ac_full_price
      t.text :description
      t.integer :product_type_id
      t.integer :business_id
      t.string :unit
      t.float :price

      t.timestamps
    end
    add_index :products, :category_id
    add_index :products, :product_type_id
    add_index :products, :business_id
  end
end
