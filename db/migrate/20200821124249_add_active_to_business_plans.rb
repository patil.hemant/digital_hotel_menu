class AddActiveToBusinessPlans < ActiveRecord::Migration[5.1]
  def change
    add_column :business_plans, :active, :boolean, default: true
  end
end
