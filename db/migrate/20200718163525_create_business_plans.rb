class CreateBusinessPlans < ActiveRecord::Migration[5.1]
  def change
    create_table :business_plans do |t|
      t.integer :business_id
      t.integer :plan_id

      t.timestamps
    end
    add_index :business_plans, :business_id
    add_index :business_plans, :plan_id
  end
end
