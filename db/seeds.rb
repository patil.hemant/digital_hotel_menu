# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


plans = [
{
name: 'Monthly',
amount: 199,
description: 'Bigginer Pack',
no_of_days: 30,
no_of_printable_qrcodes: 15
},
{
name: 'Half Yearly',
amount: 1199,
description: 'Descent Pack',
no_of_days: 30,
no_of_printable_qrcodes: 15
},
{
name: 'Annually',
amount: 1999,
description: 'Popilar Pack',
no_of_days: 30,
no_of_printable_qrcodes: 15
}
]

plans.each do |plan|
Plan.create(plan)
end