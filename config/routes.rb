Rails.application.routes.draw do
  resources :plans
  resources :purchases
  resources :businesses do
    collection do
      get 'show_new'=>'businesses#show_new'
    end
    resources :categories
    resources :products
  end
  get 'home/index'
  get 'home1'=>'home#home_1'
  get 'new_products'=>'products#new_index'
  root 'home#index'
  devise_for :users, :controllers => {:registrations => "registrations"}
  resource :user, only: [:edit] do
	  collection do
	    patch 'update_password'
	  end
	end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
